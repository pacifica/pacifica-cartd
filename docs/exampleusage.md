# Example Usage

Every cart has a unique ID associated with it. For the examples
following we used a uuid generated by standard Linux utilities.

```
MY_CART_UUID=`uuidgen`
```

## Create a Cart

Post a file to create a new cart.

Contents of file (foo.json).

id =  the id being used on the Archive

path = internal structure of bundle for file placement

hashtype = hashlib hashtype used to generate hashsum

hashsum = the hash (hex value) of the file using the hashtype listed

```
{
  "fileids": [
    {"id":"foo.txt", "path":"1/2/3/foo.txt", "hashtype":"md5", "hashsum":""},
    {"id":"bar.csv", "path":"1/2/3/bar.csv", "hashtype":"md5", "hashsum":""},
    {"id":"baz.ini", "path":"2/3/4/baz.ini", "hashtype":"md5", "hashsum":""}
  ]
}
```

Post the file to the following URL.
```
curl -X POST --upload-file /tmp/foo.json http://127.0.0.1:8081/$MY_CART_UUID
```

## Status a Cart

Head on the cart to find whether its created and ready for download.

```
curl -I -X HEAD http://127.0.0.1:8081/$MY_CART_UUID
```

Will receive headers back with the specific data needed. These are:

'X-Pacifica-Status'
'X-Pacifica-Message'

Message will be blank if there is no error.
The list of possible status:

If the cart is waiting to be processed and there is no current state.
  "X-Pacifica-Status": "waiting"


If the cart is being processed and waiting for files to be staged locally.
  "X-Pacifica-Status": "staging"


If the cart has the files locally and is currently creating the tarfile.
  "X-Pacifica-Status": "bundling"

If the cart is finally ready for download.
  "X-Pacifica-Status": "ready"

If the cart has an error (such as no space available to create the tarfile).
  "X-Pacifica-Status": "error"
  "X-Pacifica-Message": "No Space Available"

## Get a cart

To download the tarfile for the cart.

```
curl http://127.0.0.1:8081/$MY_CART_UUID?filename=my_cart.tar

In the above url my_cart.tar can be any file name of your choice
If no filename parameter is present you will get back data_date.tar in the form data_YYYY_MM_DD_HH_MM_SS.tar
```
To save to file
```
curl -O -J http://127.0.0.1:8081/$MY_CART_UUID?filename=my_cart.tar

-O says to save to a file, and -J says to use the Content-Disposition file name the server is trying to send back

Once this finishes there will be a tar file named my_cart.tar
Untar by:

tar xf my_cart.tar
```

## Delete a Cart

Delete a created cart.

```
curl -X DELETE http://127.0.0.1:8081/$MY_CART_UUID
```

Data returned should be json telling you status of cart deletion.
